import 'dart:ui';

abstract class ColorsApp{

  abstract final Color text;
  abstract final Color accent;
  abstract final Color textAccent;
  abstract final Color disableAccent;
  abstract final Color disableTextAccent;
  abstract final Color hint;
  abstract final Color googleOAth2;
  abstract final Color iconTint;
  abstract final Color background;
  abstract final Color error;
  abstract final Color block;
  abstract final Color subText;

}

class LightColors extends ColorsApp {  @override
  // TODO: implement asset
  Color get accent => const Color(0x6A8BF9);

  @override
  // TODO: implement background
  Color get background => const Color(0xFFFFFF);

  @override
  // TODO: implement block
  Color get block =>  const Color(0xFFFFFF);

  @override
  // TODO: implement disableAccent
  Color get disableAccent => const Color(0xA7A7A7);

  @override
  // TODO: implement disableTextAccent
  Color get disableTextAccent =>  const Color(0xFFFFFF);

  @override
  // TODO: implement error
  Color get error => const Color(0xFF0000);

  @override
  // TODO: implement googleOAth2
  Color get googleOAth2 => const Color(0x00ec8000);

  @override
  // TODO: implement hint
  Color get hint => const Color(0x00cfcfcf);

  @override
  // TODO: implement iconTint
  Color get iconTint => Color(0x141414);

  @override
  // TODO: implement subText
  Color get subText => Color(0x818181);

  @override
  // TODO: implement text
  Color get text => Color(0x3a3a3a);

  @override
  // TODO: implement textAccent
  Color get textAccent => Color(0xFFFFFF);
}
