import 'dart:js';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ses_sion_1_3/common/theme.dart';
import 'package:ses_sion_1_3/auth/presentation/pages/run.dart';

class CustomTextField extends StatefulWidget {
  final String title;
  final String hint;
  final TextEditingController controller;
  final bool eye;
  final Function(String)? onChange;

  const CustomTextField(
      {super.key,
      required this.title,
      required this.hint,
      required this.controller,
      this.eye = false,
      this.onChange});

  @override
  State<CustomTextField> createState() => _CustomTextFieldState();
}

class _CustomTextFieldState extends State<CustomTextField> {

  bool isVisible = true;
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const SizedBox(
          height: 24
        ),
        Text(
          widget.title,
          style: Theme.of(context).textTheme.labelMedium,
        ),
        const SizedBox(
          height: 8,
        ),
        SizedBox(
          width: double.infinity,
          child: TextField(
            obscureText: (widget.eye) ? isVisible : false,
            controller: widget.controller,
            obscuringCharacter: "*",
            onChanged: widget.onChange,
            decoration: InputDecoration(
              contentPadding: const EdgeInsets.only(top: 14, bottom: 14, left: 10),
              hintText: widget.hint,
              hintStyle: Theme.of(context).textTheme.titleMedium,
              suffixIcon: GestureDetector(
                onTap: (){
                  setState(() {
                    isVisible = !isVisible;
                  });
                },
                child: Image.asset("accets/eye-slash.png"),
              )
            ),
          ),
        )
      ],
    );
  }
}
