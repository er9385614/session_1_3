import 'package:flutter/material.dart';
import 'package:ses_sion_1_3/common/colors.dart';


final colorsLight = LightColors();


var theme = ThemeData(
  textTheme: TextTheme(
    titleLarge: TextStyle(
      color: colorsLight.text,
      fontWeight: FontWeight.w700,
      fontSize: 24,
      fontFamily: "Roboto"
    ),
    titleMedium: TextStyle(
      color: colorsLight.subText,
      fontFamily: "Roboto",
      fontSize: 14,
      fontWeight: FontWeight.w500
    ),
    titleSmall: TextStyle(
      color: colorsLight.disableTextAccent,
      fontWeight: FontWeight.w700,
      fontSize: 16,
      fontFamily: "Roboto"
    )
  ),
  filledButtonTheme: FilledButtonThemeData(
    style: FilledButton.styleFrom(
      textStyle: TextStyle(fontWeight: FontWeight.bold),
      padding: EdgeInsets.symmetric(horizontal: 36, vertical: 24),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(4)),
      backgroundColor: colorsLight.accent,
      foregroundColor: colorsLight.disableTextAccent,
      disabledForegroundColor: colorsLight.disableTextAccent,
      disabledBackgroundColor: colorsLight.disableAccent
    )
  )
);