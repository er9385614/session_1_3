import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ses_sion_1_3/auth/presentation/pages/signUpPage.dart';
import 'package:ses_sion_1_3/common/theme.dart';
import 'package:supabase_flutter/supabase_flutter.dart';

import '../../../common/colors.dart';

void main() async{
  await Supabase.initialize(
      url: "",
      anonKey: "");
  runApp(MyApp());
}


class MyApp extends StatefulWidget {
  MyApp({super.key});

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {


  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData.light(),
      home: SignUpPage(),
    );
  }
}